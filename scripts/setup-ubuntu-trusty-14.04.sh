#!/bin/bash

# Update apt repo
sudo apt-get update && sudo apt-get upgrade

# Install Docker
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" > /etc/apt/sources.list.d/docker.list
sudo apt-get update
sudo apt-get install docker-engine

# Setup nodejs
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

# Install instructions
echo "Run the following command from the project root to complete:"
echo "	npm install && (cd src/server && typings install) && (cd src/client && typings install)"
