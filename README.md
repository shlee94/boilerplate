# Boilerplate

This project is designed as a Boilerplate for web apps that use a common technology stack:

- TypeScript
- Babel
- Gulp
- Sass
- Node.js
- Express.js
- React
- Redux
- Inversify
- Docker
- Mocha
- Enzyme
- Webpack

## Running the project

Most of the development scripts are wrapped in an `npm` script. Use `npm start` to run the application.

### TypeScript

TypeScript is the language for is the primary language for this project, on both the client and the server.
Compilation is done through `gulp` and `gulp-typescript`.

### Babel

JavaScript transpiler. Allows the use of next generation JavaScript functionality, such as `async`.
NOTE: Although Babel is set up in this boilerplate, the default build target is ES5 (since ES6 does not yet work with `node`).  

### Gulp

The task runner for the project. Used to build (and eventually test) the project. All of the `gulp` commands are wrapped as `npm` scripts,
so running `npm run build` will build the project, and `npm run watched-build` will start an incremental build.

### SASS

The CSS preprocessor.

### Node.js

The server-side runtime environment for the project.

### Express.js

The web server framework. A nice framework for creating a webserver in Node.js. There is a barebones express server in the project (`app.ts`).

### React

The UI/View framework for the client.

### Redux

The state management framework.

### Inversify

An IoC framework for dependency injection.

### Mocha

The testing framework/test runner for JavaScript. Not yet configured.

### Enzyme

The testing framework for React components. Not yet configured.

### Docker

System to create and manage containers (i.e. environments for applications). Currently, this is used for production and test deployments, but eventually it may be useful
to have a Docker container to use as a development environment as well. Not properly configured for incremental builds when developing, so development should be done on a
regular local deployment. Building a production/test image is done by `docker build -t <tag_name> images/Dockerfile .`, and creating a container is done by `docker run --name <container_name> <tag_name>`,
using the `-p <container_port>:<host_port>` to map a host port to the container port.

### Webpack

Bundling.
