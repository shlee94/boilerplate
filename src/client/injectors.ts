/**
* Initializes and manages the dependency injection system.
*/

import getDecorators from "inversify-inject-decorators";
import { Kernel } from "inversify";

// Define the symbols used to get dependecies
let TYPES = {
	// Example:
    // UserService: "UserService"
}

// Map the symbols to the implementation
let kernel = new Kernel();
// Bind the type to the injectable dependency
// Example:
// kernel.bind<UserService>(TYPES.UserService).to(FakeUserService);

let {
    lazyInject,
    lazyInjectNamed,
    lazyInjectTagged,
    lazyMultiInject
} = getDecorators(kernel);

export {
	kernel,
    TYPES,
	lazyInject,
	lazyInjectNamed,
	lazyInjectTagged,
	lazyMultiInject
}
