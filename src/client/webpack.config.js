var webpack = require("webpack");

module.exports = {
    entry: ["babel-polyfill", "./main.tsx"],
    output: {
        filename: "../../dist/client/bundle.js",
    },

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        loaders: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
			// Loaders execute right to left
            { test: /\.tsx?$/, loaders: ["babel-loader", "ts-loader"] },
            // '.scss' files will be loaded with 'sass-loader'
            { test: /\.scss$/, loaders: ["style-loader", "css-loader", "sass-loader"]}
        ]
    },

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    }
};
