import * as assert from "assert"
import { suite, test } from "mocha-typescript"

@suite
class SampleTest {
    @test
    "basic addition should work"() {
        assert.equal(2 + 2, 4);
    }
}
