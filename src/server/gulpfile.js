/**
* Builds the server-side code for the project.
* Supports incremental/watched build with 'gulp watch'
*/

var gulp = require('gulp');
var ts = require('gulp-typescript');
var babel = require('gulp-babel');

var tsProject = ts.createProject('tsconfig.json');

// Default gulp task to build server
gulp.task('default', function() {
	return tsProject.src()
		.pipe(tsProject())
		.js
		.pipe(babel())
		.pipe(gulp.dest('../../dist/server'));
});

// Watched version of the build server task
gulp.task('watch', ['default'], function() {
	gulp.watch('./**/*.ts', ['default']);
});
