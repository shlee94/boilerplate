/**
* The entry point for the server code. Barebones webserver.
*/

import * as express from 'express';
import * as path from 'path';

let port: number = process.env.PORT || 3000;
let app = express();

//
// Path constants
//
const PROJ_ROOT: string = path.resolve(__dirname, '../..');
const DIST_ROOT: string = path.resolve(PROJ_ROOT, 'dist');
const PUBLIC_ROOT: string = path.resolve(PROJ_ROOT, 'public');
const CLIENT_ROOT: string = path.resolve(DIST_ROOT, 'client');
const SERVER_ROOT: string = path.resolve(DIST_ROOT, 'server');


//
// Setup statics routes
//

// Path to npm packages
app.use('/lib', express.static(path.resolve(PROJ_ROOT, 'node_modules')));

// Path to client code
app.use('/client', express.static(CLIENT_ROOT));

// Path to public files
app.use('/public', express.static(PUBLIC_ROOT))


//
// Route to index
//
app.get('/', (req: express.Request, res: express.Response) => {
    res.sendFile(path.resolve(PUBLIC_ROOT, 'views/index.html'));
});

let server = app.listen(port, () => {
    let host = server.address().address;
    let port = server.address().port;
    console.log('This express app is listening on port:' + port);
});
